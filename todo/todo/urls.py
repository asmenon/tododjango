import django
from django.contrib.auth import logout
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings
from django.views.generic.base import TemplateView
from todo_app.views import (
    todo_view,
    add_todo_list,
    complete_todo,
    delete_complete_todo,
    delete_all,
    index,
    register,
    login_page,
    logout_page,
)


def bad(request):
    """Simulates a server error"""
    1 / 0


urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
]


urlpatterns += i18n_patterns(
    path("bad/", bad),
    path("", index, name="index"),
    path(f"{settings.ADMIN_URL}/", admin.site.urls),
    path("user/", include("user.urls", namespace="user")),
    path("api/v1/", include("user.api.urls", namespace="user_api")),
    path("todo/", todo_view, name="todo"),
    path("add-todo-list/", add_todo_list, name="add"),
    path("complete-todo/<todo_item>", complete_todo, name="complete"),
    path("delete-complete-todo/", delete_complete_todo, name="delete-complete"),
    path("delete-all-todo", delete_all, name="delete-all"),
    path("register/", register, name="register"),
    path("login/", login_page, name="login"),
    path("logout/", logout_page, name="logout"),
    path("api-test/", include("todo_app.urls", namespace="todo_app")),
)

if settings.DEBUG:
    urlpatterns += [
        # Testing 404 and 500 error pages
        path("404/", TemplateView.as_view(template_name="404.html"), name="404"),
        path("500/", TemplateView.as_view(template_name="500.html"), name="500"),
    ]

    from django.conf.urls.static import static

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    import debug_toolbar

    urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]
