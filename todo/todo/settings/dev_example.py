import os

from .base import *  # noqa


DEBUG = True
ALLOWED_HOSTS = ["*"]
DEV = DEBUG

INSTALLED_APPS += ("debug_toolbar", "todo_app.apps.TodoAppConfig", "captcha")

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "todo.db",
    }
}

MIDDLEWARE += ("debug_toolbar.middleware.DebugToolbarMiddleware",)

SECRET_KEY = "devel"

# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

SITE_ID = 2

AUTH_PASSWORD_VALIDATORS = []
