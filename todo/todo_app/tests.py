from django.test import TestCase
from .models import TodoItem, Creator
import datetime

# Create your tests here.


class TodoItemModelTests(TestCase):
    def setUp(self) -> None:
        # Setup test cases.
        test_creator = Creator.objects.create(
            name="testcreator", email="testcreator@foo.com"
        )
        TodoItem.objects.create(
            text="todo #1",
            creator=test_creator,
            expected_date=datetime.date(2021, 12, 12),
        )
        TodoItem.objects.create(
            text="todo #2",
            creator=test_creator,
            expected_date=datetime.date(2019, 11, 11),
        )

    # def test_todo_is_not_default_value(self):
    #     """Ensures no entry in the TodoItem db does not have a default value for the text field."""
    #     todo_items = TodoItem.objects.all()
    #     for item in todo_items:
    #         self.assertTrue(item.text != "default todo")

    def test_todo_is_within_due_date(self):
        t1 = TodoItem.objects.get(text="todo #1")
        t2 = TodoItem.objects.get(text="todo #2")
        self.assertFalse(t1.is_within_due_date())
        self.assertTrue(t2.is_within_due_date())

    def test_todo_is_date_expired(self):
        t1 = TodoItem.objects.get(text="todo #1")
        t2 = TodoItem.objects.get(text="todo #2")
        self.assertFalse(t1.check_date_expired())
        self.assertTrue(t2.check_date_expired())
