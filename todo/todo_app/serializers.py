from django.db import models
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField
from rest_framework.response import Response
from .models import TodoItem, Creator
from datetime import datetime


class TodoListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        if len(data) != 0 and "query" in self.context:
            # Do filtering
            days = None
            if "days" in self.context.get("query"):
                try:
                    days = int(self.context.get("query")["days"])
                except ValueError:
                    days = None
                    print("Error parsing query param days in TodoListSerializer")
            if days is not None:
                print("IN QUERY LIST SERIALIZER")
                data = self.sort_data(data, days)
            return super().to_representation(data)
        elif "request" in self.context:
            if "sort" in self.context.get("request").query_params:
                sort_type = self.context.get("request").query_params["sort"]
                print(sort_type)
                print(data)
                data.sort(
                    key=lambda x: x.expected_date,
                    reverse=True if sort_type == "desc" else False,
                )
            return super().to_representation(data)
        else:
            print("not serializing")
            return super().to_representation(data)

    def sort_data(self, data, days):
        to_remove = []
        for item in data:
            if not item.is_within_due_date(days):
                to_remove.append(item.text)
        return data.exclude(text__in=to_remove)


class TodoItemSerializer(serializers.ModelSerializer):
    class Meta:
        list_serializer_class = TodoListSerializer
        model = TodoItem
        fields = (
            "text",
            "expected_date",
        )

    def create(self, validated_data):
        creator_instance = Creator.objects.get(
            name=self.context["request"].user.username
        )
        todo_instance = TodoItem.objects.create(
            text=validated_data["text"],
            creator=creator_instance,
            expected_date=validated_data["expected_date"],
        )
        todo_instance.save()
        serializer = TodoItemSerializer(
            TodoItem.objects.filter(creator=creator_instance)
        )
        return Response(serializer.data)


class CreatorSerializer(serializers.ModelSerializer):
    # todo_list = TodoItemSerializer(many=True)
    todo_list = SerializerMethodField(source="get_todo_list")

    class Meta:
        model = Creator
        fields = (
            "name",
            "todo_list",
        )
        extra_kwargs = {"name": {"validators": []}}

    def get_todo_list(self, obj):
        return TodoItemSerializer(
            obj.todo_list.all(),
            many=True,
            context={"query": self.context.get("request").query_params},
        ).data

    # Valid functionality to add a list of items.
    # def create(self, validated_data):
    #     print("IN Creator Serializer create(..)")
    #     todo_data = validated_data
    #     print(validated_data)
    #     creator_instance = Creator.objects.get(name=todo_data["name"])
    #     for todo_item in todo_data["todo_list"]:
    #         todo_instance = TodoItem.objects.create(
    #             text=todo_item["text"],
    #             creator=creator_instance,
    #             expected_date=todo_item["expected_date"],
    #         )
    #         todo_instance.save()
    #     serializer = CreatorSerializer(creator_instance)
    #     print(serializer.data)
    #     return Response(serializer.data)
