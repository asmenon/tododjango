from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
user = get_user_model()


class TodoForm(forms.Form):
    text = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class': 'form-control',
                                                                        'placeholder': 'Enter todo e.g. Delete junk files',
                                                                        'aria-label': 'Todo',
                                                                        'aria-describedby': 'add-btn'}))
    time = forms.DateField(widget=forms.TextInput(attrs={
                           'id': 'datepicker', 'class': 'form-control', 'placeholder': 'Enter expected completion date'}))


class CreateUserForm(UserCreationForm):
    class Meta:
        model = user
        fields = ['username', 'email', 'password1', 'password2']
