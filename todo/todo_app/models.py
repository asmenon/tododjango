import datetime
from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model

user = get_user_model()
# Create your models here.


class Creator(models.Model):
    name = models.CharField(max_length=60, unique=True)
    email = models.CharField(max_length=200, null=True)

    class Meta:
        app_label = "todo_app"


class TodoItem(models.Model):
    """Model for a todo item."""

    text = models.CharField(max_length=60, default="default todo")
    is_complete = models.BooleanField(default=False)
    creator = models.ForeignKey(
        "Creator", related_name="todo_list", on_delete=models.CASCADE
    )
    expected_date = models.DateField(null=True)

    class Meta:
        app_label = "todo_app"

    def __str__(self):
        return self.text

    def check_date_expired(self):
        """
        Checks if expected_date field has passed 'today'.
        Returns True if expected_date is in the past.
        Returns False otherwise.
        """
        if self.expected_date == None:
            return False
        current_date = datetime.date.today()
        if current_date < self.expected_date:
            return False
        else:
            return True

    def is_within_due_date(self, tolerance=3):
        """
        Is within due date returns true if the expected date is within 'tolerance' days from the current date.
        Returns False if the absolute value of the difference between expected date and today is greater than tolerance.
        Returns True otherwise.
        """
        if self.expected_date == None:
            return False
        current_date = datetime.date.today()
        if (self.expected_date - current_date).days <= tolerance:
            return True
        else:
            return False
