from django.contrib.auth import get_user_model
from django.db.models import query
from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST
from rest_framework import authentication
from rest_framework.response import Response
from .models import TodoItem, Creator
from .forms import TodoForm, CreateUserForm
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authentication import BasicAuthentication
from .serializers import CreatorSerializer, TodoItemSerializer

user = get_user_model()

# Create your views here.


class TodoViewSet(viewsets.ModelViewSet):
    queryset = TodoItem.objects.all()
    serializer_class = TodoItemSerializer
    permission_classes = [
        IsAuthenticated,
    ]
    authentication_classes = [
        BasicAuthentication,
    ]

    def get_queryset(self):
        queryset = TodoItem.objects.all()
        creator_instance = Creator.objects.get(name=self.request.user.username)
        return TodoItem.objects.filter(creator=creator_instance)


class CreatorViewSet(viewsets.ModelViewSet):
    queryset = Creator.objects.all()
    serializer_class = CreatorSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]
    authentication_classes = [
        BasicAuthentication,
    ]

    def get_queryset(self):
        queryset = Creator.objects.all()
        creator_name = None
        if "username" in self.request.query_params:
            creator_name = self.request.query_params.get("username")
        if creator_name is not None:
            queryset = Creator.objects.filter(name=creator_name)
        return queryset


def index(request):
    """Simply redirect to the login page."""
    return redirect("login")


def login_page(request):
    """View that handles user authentication using django auth"""
    if request.user.is_authenticated:
        return redirect("todo")
    else:
        if request.method == "POST":
            username = request.POST.get("username")
            password = request.POST.get("password")
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("todo")
            else:
                messages.info(request, "Invalid credentials")
    return render(request, "login.html")


def logout_page(request):
    """Simply logout the given user."""
    logout(request)
    return redirect("login")


def register(request):
    """Request new credentials from user to fill out 'CreateUserForm' for registration in database."""
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get("username")
            return redirect("login")
    context = {"form": form}
    return render(request, "register.html", context)


@login_required(login_url="login")
def todo_view(request):
    notification_calls()
    """Main landing page view that requires a logged in user. Lists all the users todo objects."""
    new_creator = Creator.objects.get_or_create(name=request.user.username)
    # new_creator.save()
    all_todo_items = TodoItem.objects.filter(creator__name=request.user.username)
    to_delete = []
    for item in all_todo_items:
        if item.check_date_expired():
            to_delete.append(item.text)
    all_todo_items = TodoItem.objects.filter(
        creator__name=request.user.username, text__in=to_delete
    ).delete()
    all_todo_items = TodoItem.objects.filter(creator__name=request.user.username)
    form = TodoForm()
    return render(
        request, "todo_landing.html", {"all_items": all_todo_items, "form": form}
    )


@require_POST
@login_required(login_url="login")
def add_todo_list(request):
    """Function that handles adding to the list of todo items for the logged in user."""
    form = TodoForm(request.POST)

    if form.is_valid():
        new_creator = _get_creator(request.user.username)
        exp_date = request.POST["time"]
        date_formatted = _format_date(exp_date)
        new_item = TodoItem.objects.create(
            text=request.POST["text"], creator=new_creator, expected_date=date_formatted
        )
        new_item.save()
    return redirect("todo")


@login_required(login_url="login")
def complete_todo(request, todo_item):
    """Function that marks items in the todo list as complete" for the logged in user."""
    new_creator = _get_creator(request.user.username)
    todo_update = TodoItem.objects.filter(creator=new_creator).get(pk=todo_item)
    print(todo_update.text)
    todo_update.is_complete = True
    todo_update.save()
    return redirect("todo")


@login_required(login_url="login")
def delete_complete_todo(request):
    new_creator = _get_creator(request.user.username)
    """Function that handles deleting completed todo items for the logged in user."""
    TodoItem.objects.filter(is_complete__exact=True, creator=new_creator).delete()
    return redirect("todo")


@login_required(login_url="login")
def delete_all(request):
    new_creator = _get_creator(request.user.username)
    """Function that handles deleting all todo items for the logged in user."""
    TodoItem.objects.filter(creator=new_creator).delete()
    return redirect("todo")


def _get_creator(creator_name: str) -> Creator:
    """Helper function that returns the creator object for the logged in user."""
    new_creator = Creator.objects.get_or_create(name=creator_name)
    if new_creator is None:
        new_creator = Creator(name=creator_name)
        new_creator.save()
    else:
        new_creator = new_creator[0]
    return new_creator


def _format_date(exp_date):
    # Default format is mm/dd/yyyy. Converting to YYYY-MM-DD
    year = ""
    day = ""
    month = ""
    split_date = exp_date.split("/")
    month = split_date[0]
    day = split_date[1]
    year = split_date[2]
    final_format = year + "-" + month + "-" + day
    return final_format


def notification_calls():
    users = user.objects.all()
    email_datatuple = []
    for item in users:
        send_email = False
        email_str = ""
        email_recipient_id = item.email
        all_user_items = TodoItem.objects.filter(creator__name=item)
        for user_item in all_user_items:
            if user_item.is_within_due_date():
                send_email = True
                email_str = email_str + user_item.text + ", "
        if send_email:
            email_str = str(item) + ", You have some upcoming todo's due - " + email_str
            message = ("Todo app notification!", email_str, None, [email_recipient_id])
            send_mail(
                "TodoAppNotification",
                email_str,
                None,
                [email_recipient_id],
                fail_silently=False,
            )
